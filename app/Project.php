<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    public $fillable = [
        'title',
        'slug',
        'md_body',
        'image',
    ];

}
