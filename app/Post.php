<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public $fillable = [
        'title',
        'slug',
        'md_body',
        'published',
    ];

}
