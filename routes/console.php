<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('wanikani', function() {
    $key = env('WANIKANI_API_KEY', false);
    if ($key === false) {
        echo 'No WaniKani API found in .env.';
        exit;
    }

    $url_base = "https://www.wanikani.com/api/user/{$key}/";

    $queries = [
        'level_progression' => 'level-progression',
        'srs_distribution' => 'srs-distribution',
        'recent_unlocks' => 'recent-unlocks/20',
        'critical_items' => 'critical-items/90'
    ];

    $data = [];

    foreach ($queries as $key => $query) {
        $response = file_get_contents($url_base.$query);
        $jdata = json_decode($response);

        if (!isset($data['user_information'])) {
            $data['user_information'] = $jdata->user_information;
        }

        $data[$key] = $jdata->requested_information;
    }

    Storage::put('wanikani.json', json_encode($data));

})->describe('Update wanikani data');
