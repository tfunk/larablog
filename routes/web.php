<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Post;


Route::get('', function () {
    $posts = Post::where('published', true)->orderBy('created_at', 'desc')->limit(3)->get();
    return view('index', compact('posts'));
});

Route::resource('projects', 'ProjectsController', ['only' => [
    'index', 'show'
]]);

Route::resource('blog', 'PostsController', ['only' => [
    'index', 'show'
]]);

Route::get('japanese', function() {
    $data = [];
    if (Storage::exists('wanikani.json')) {
        $contents = Storage::get('wanikani.json');
        $data = json_decode($contents);
    }
    return view('japanese', compact('data'));
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'auth'], function() {
    Route::get('', 'HomeController@index');
    Route::resource('projects', 'ProjectsController');
    Route::resource('posts', 'PostsController');
});

Auth::routes();
