$(function() {
	$('.nav-toggle').on('click', function() {
		$(this).toggleClass('is-active');
		$(this).next('.nav-menu').toggleClass('is-active');
	});
});
