<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title') | suchprogrammer.net </title>

		<!-- Favicon city -->
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-32x32.png') }}" sizes="32x32">
		<link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-16x16.png') }}" sizes="16x16">
		<link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">
		<link rel="mask-icon" href="{{ asset('/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
		<link rel="shortcut icon" href="{{ asset('/favicon/favicon.ico') }}">
		<meta name="msapplication-config" content="{{ asset('/favicon/browserconfig.xml') }}">
		<meta name="theme-color" content="#ffffff">

		<link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
	</head>
	<body>
		@include('shared.nav')

		@yield('content')

		@include('shared.footer')

		<script src="/js/analytics.js" async defer></script>
		<script src="{{ asset('js/app.js') }}" type="text/javascript" async></script>
		@yield('js')
	</body>
</html>
