<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') | suchprogrammer.net admin</title>

		<link href="{{ asset('css/admin.css') }}" rel="stylesheet"/>
    </head>
    <body>
		<section class="hero">
			<div class="hero-body">
				<div class="container">
					<h1 class="title">@yield('title')</h1>
				</div>
			</div>
		</section>

		<ul>
			<li>
				<form method="POST" action="{{ route('logout') }}">
					{{ csrf_field() }}
					<button type="submit">Logout</button>
				</form>
			</li>

			<li>
				<a href="{{ route('admin.projects.index') }}">Projects</a>
			</li>

			<li>
				<a href="{{ route('admin.posts.index') }}">Blog Posts</a>
			</li>
		</ul>

		@yield('content')

		<script src="{{ asset('js/admin.js') }}"></script>
		@yield('js')
    </body>
</html>
