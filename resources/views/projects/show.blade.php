@extends('layouts.front')

@section('title', $project->title)

@section('content')
	<section class="hero">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">{{ $project->title }}</h1>
			</div>
		</div>
	</section>

	<section class="section projects">
		<div class="container">

			<div class="content">
				<a href="{{ route('projects.index') }}">&larr; Back to projects</a><br><br>

				{!! $project->body !!}
			</div>

			<a target="_blank" href="https://logorrhea.itch.io/{{ $project->slug }}" title="itch.io link">Check it out on itch.io</a>
		</div>
	</section>
@endsection
