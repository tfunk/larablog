@extends('layouts.front')

@section('title', 'Projects')

@section('content')
	<section class="hero">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">Projects</h1>
			</div>
		</div>
	</section>

	<section class="section projects">
		<div class="container">
			<div class="columns is-multiline">


				@foreach($projects as $project)
					<div class="column is-4-desktop is-6-tablet">
						<div>
							<a href="{{ route('projects.show', $project->slug) }}">
								<figure class="image">
									<img src="{{ $project->image }}" alt="Image">
									<figcaption class="title is-4">{{ $project->title }}</figcaption>
								</figure>
							</a>
						</div>
					</div>
				@endforeach

			</div>
		</div>
	</section>
@endsection
