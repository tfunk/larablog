{{ csrf_field() }}

<label for="title">Title</label><br>
<input id="title" name="title" type="text" value="{{ $project->title }}" size="100"/>
<br><br>

<label for="slug">Slug</label><br>
<input id="slug" name="slug" type="text" value="{{ $project->slug }}" size="100"/>
<br><br>

<label for="md_body">Summary</label><br>
<textarea cols="80" id="md_body" name="md_body" rows="20">{{ $project->md_body }}</textarea>
<br><br>

<label for="image">Image</label><br>
<input id="image" name="image" type="text" value="{{ $project->image }}" size="100"/>
<br><br>

