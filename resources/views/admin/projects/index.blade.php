@extends('layouts.admin')

@section('title', 'Projects')

@section('content')
	<table>
		<colgroup>
			<col class="title"/>
			<col class="manage"/>
		</colgroup>

		<thead>
			<tr>
				<th>Title</th>
				<th>Manage</th>
			</tr>
		</thead>

		<tbody>
			@foreach($projects as $project)
				<tr>
					<td>{{ $project->title }}</td>
					<td>
						<a href="{{ route('admin.projects.edit', $project) }}">Edit</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection
