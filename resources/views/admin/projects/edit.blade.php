@extends('layouts.admin')

@section('title', $project->title)

@section('content')
	<form method="POST" action="{{ route('admin.projects.update', $project) }}" enctype="multipart/form-data">
		{{ method_field('PUT') }}

		@include('admin.projects._fields')

		<button type="submit">Update</button>
	</form>
@endsection
