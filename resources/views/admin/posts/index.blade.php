@extends('layouts.admin')

@section('title', 'Posts')

@section('content')
	<a href="{{ route('admin.posts.create') }}">New Blog Post</a>

	<table>
		<colgroup>
			<col class="title"/>
			<col class="date"/>
			<col class="manage"/>
		</colgroup>

		<thead>
			<tr>
				<th>Title</th>
				<th>Date</th>
				<th>Manage</th>
			</tr>
		</thead>

		<tbody>
			@foreach($posts as $post)
				<tr>
					<td>{{ $post->title }}</td>
					<td>{{ $post->created_at }}</td>
					<td>
						<a href="{{ route('admin.posts.edit', $post) }}">Edit</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection
