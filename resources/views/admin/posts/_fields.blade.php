{{ csrf_field() }}

<label for="title">Title</label><br>
<input name="title" id="title" type="text" value="{{ $post->title }}"/>
<br><br>

<label for="slug">Slug</label><br>
<input name="slug" id="slug" type="text" value="{{ $post->slug }}"/>
<br><br>

<label for="title">Title</label><br>
<textarea id="md_body" cols="80" id="md_body" name="md_body" rows="20">{{ $post->md_body }}</textarea>
<br><br>

<label><input name="published" type="checkbox" value="true" {{ $post->published?'checked="checked"':'' }}/>Published</label>
<br><br>
