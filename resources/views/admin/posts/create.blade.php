@extends('layouts.admin')

@section('title', 'New Blog Post')

@section('content')
	<form method="POST" action="{{ route('admin.posts.store') }}">

		@include('admin.posts._fields')

		<button type="submit">Create Post</button>
	</form>
@endsection
