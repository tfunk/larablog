@extends('layouts.admin')

@section('title', $post->title)

@section('content')
	<form method="POST" action="{{ route('admin.posts.update', $post) }}">
		{{ method_field('PUT') }}

		@include('admin.posts._fields')

		<button type="submit">Update Post</button>
	</form>
@endsection
