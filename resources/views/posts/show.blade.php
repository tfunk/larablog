@extends('layouts.front')

@section('title', $post->title)

@section('content')
	<section class="hero">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">{{ $post->title }}</h1>
				<h4 class="subtitle">{{ $post->created_at->diffForHumans() }}</h4>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="container">
			<div class="columns">
				<div class="column is-8">
					<div class="content">
						<a href="{{ route('blog.index') }}">&larr; Back to blog</a><br><br>

						{!! $post->body !!}
					</div>
				</div>

				<div class="column is-4 is-hidden">

					<div class="box content">
						<p class="title is-3">Archives</p>

						<ul>
							@for($i=0;$i<4;$i++)
								<li><a href="">{{ intval(date('Y'))-1-$i }}</a></li>
							@endfor
						</ul>
					</div>

				</div>
			</div>
		</div>
	</section>
@endsection
