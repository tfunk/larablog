@extends('layouts.front')

@section('title', 'Blog')

@section('content')
	<section class="hero">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">Blog</h1>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="container">
			<div class="columns">
				<div class="column is-8">
					<div>
						<ul class="post-list">
							@foreach($posts as $post)
								<li>
									<p class="title is-4"><a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a></p>
									<p class="subtitle is-6">{{ $post->created_at->diffForHumans() }}</p>
								</li>
							@endforeach
						</ul>
					</div>
				</div>

				<div class="column is-4 is-hidden">

					<div class="box content">
						<p class="title is-3">Archives</p>

						<ul>
							@for($i=0;$i<4;$i++)
								<li><a href="">{{ intval(date('Y'))-1-$i }}</a></li>
							@endfor
						</ul>
					</div>

				</div>
			</div>
		</div>
	</section>
@endsection
