@extends('layouts.front')

@section('title', '日本語の練習')

@section('content')
	<section class="hero">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">日本語の練習</h1>
				<h2 class="subtitle">Japanese practice data from <a target="_blank" href="https://wanikani.com/users/tyfoo">WaniKani</a></h2>
			</div>
		</div>
	</section>

  <section class="section">
		<div class="container">
			<p class="title has-text-centered">Overall Progress</p>
			<nav class="level">

				@foreach(['apprentice', 'guru', 'master', 'enlighten', 'burned'] as $level)
					<div class="level-item has-text-centered">
						<div>
							<p class="title">{{ $data->srs_distribution->{$level}->total }}</p>
							<p class="heading">{{ title_case($level) }}</p>
						</div>
					</div>
				@endforeach

			</nav>
		</div>
	</section>

  <section class="section">
      <div class="container">

          <p class="title has-text-centered">Level {{ $data->user_information->level }} Progress</p>

          <p class="title is-3">Radicals</p>
          <progress class="progress" value="{{ $data->level_progression->radicals_progress }}" max="{{ $data->level_progression->radicals_total }}"></progress>

          <p class="title is-3">Kanji</p>
          <progress class="progress" value="{{ $data->level_progression->kanji_progress }}" max="{{ $data->level_progression->kanji_total }}"></progress>

      </div>
  </section>

  <section class="section">
      <div class="container">
          <div class="columns">

              <div class="column">
                  <p class="title is-3">Recent Unlocks <small class="heading">&nbsp;</small></p>

                  <table class="table">
                      <tbody>
                          @foreach($data->recent_unlocks as $row)
                          <tr>
                              <td>{{ $row->character }}</td>
                              <td class="has-text-right">{{ date('M j', $row->unlocked_date) }}</td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>

              <div class="column">
                  <p class="title is-3">Critical Items <small class="heading">"The Struggle"</small></p>

                  <table class="table">
                      <tbody>
                          @for($i = 0; $i < count($data->recent_unlocks); $i++)
                          <tr>
                              <td>{{ $data->critical_items[$i]->character }}</td>
                              <td class="has-text-right">{{ $data->critical_items[$i]->percentage }}%</td>
                          </tr>
                          @endfor
                      </tbody>
                  </table>
              </div>

          </div>
      </div>
  </section>

@endsection
