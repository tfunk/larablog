<footer class="footer">
    <div class="container">
		<div class="content has-text-centered">
			<p>I am Tyler of House Funk, Second of His Name, Wielder of Code, Biker of Paths, Father of Babby</p>
			<p>
				<a class="icon" href="mailto:tfunk@suchprogrammer.net?body=OMG hai">
					<img alt="email" src="{{ asset('img/envelope2.png') }}"/>
				</a>
				<a class="icon" href="https://logorrhea.itch.io" target="_blank">
					<img alt="itch.io" src="{{ asset('img/itch.png') }}"/>
				</a>
				<a class="icon" href="https://twitter.com/tcfunk" target="_blank">
					<img alt="twitter.com" src="{{ asset('img/twitter.png') }}"/>
				</a>
			</p>
		</div>
	</div>
</footer>
