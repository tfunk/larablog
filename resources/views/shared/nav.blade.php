<div class="container">
	<nav class="nav">
		<div class="nav-left">
			<a class="nav-item is-brand" href="{{ url('') }}">
				<div class="title">tfunk<br>
					<small class="subtitle">suchprogrammer.net</small>
				</div>
			</a>
		</div>

		<div class="nav-center">
		</div>

		<div class="nav-toggle">
			<span></span>
			<span></span>
			<span></span>
		</div>

		<div class="nav-right nav-menu">
        <!-- 
			<a class="nav-item{{ url()->current()==url('projects')?' is-active':'' }}" href="{{ url('projects') }}">projects</a>
 -->
			<a class="nav-item{{ url()->current()==url('blog')?' is-active':'' }}" href="{{ url('blog') }}">blog</a>
			<a class="nav-item{{ url()->current()==url('japanese')?' is-active':'' }}" href="{{ url('japanese') }}" title="japanese practice">日本語の練習</a>
		</div>
	</nav>
</div>
