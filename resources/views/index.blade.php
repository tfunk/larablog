@extends('layouts.front')

@section('title', 'Home')

@section('content')
<section class="section hello">
	<div class="container">
		<div class="columns">
			<div class="column is-6 alpha">
				<figure class="image"><img src="{{ asset('img/me1.png') }}" alt="Placeholder"></figure>
			</div>
			<div class="column is-6 omega">
				<h1 class="title is-1">I am Tyler Funk<span>.</span></h1>
				<div class="content">
					<p>
						I am a Web Developer and hobbyist Game Developer located in Des Moines, IA.
						I currently work as a Web Developer for Trilix Group.
					</p>

						<!--
					<dl class="horizontal">
						<dt>Email</dt>
						<dd>tfunk at fastmail dot fm</dd>
						<dt>Social</dt>
						<dd>@TODO: icons and links</dd>
					</dl>
						-->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section" id="skills">
	<div class="container">
		<div class="columns">
			<div class="column is-4">
				<h2 class="title is-2">Skills<a href="#skills" class="section-marker"><span class="is-hidden">Jump to skills</span></a></h2>
			</div>
			<div class="column is-8">
				<div class="columns">
					<div class="column is-6 content">
						<span class="title is-4"><strong>Web Development</strong></span>
						<p>PHP, JavaScript, Drupal, Ruby on Rails, Groovy, MySQL, Apache, Linux, Gulp, Webpack, Vue.js</p>
					</div>
					<div class="column is-6 content">
						<strong class="title is-4"><strong>Game Development</strong></strong>
						<p>C#, Lua, Java, Unity3D, LÖVE, GMS2</p>
					</div>
				</div>
				<div class="columns">
					<div class="column is-6 content">
						<strong class="title is-4"><strong>Other Hobbies</strong></strong>
						<p>Beginner-level Japanese, aerial silks, cyr wheel, beer brewing</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section" id="experience">
	<div class="container">
		<div class="columns">
			<div class="column is-4">
				<h2 class="title is-2">Experience<a href="#experience" class="section-marker"><span class="is-hidden">Jump to experience</span></a></h2>
				<p class="subtitle">Show me what you got. I want to see what you got.</p>
			</div>
			<div class="column is-8">
				<div class="columns">
					<div class="column is-8">
						<div class="content">
							<h3 class="title is-3">Web Developer</h3>
							<p class="subtitle">Trilix Group</p>
						</div>
					</div>
					<div class="column is-4">FEB 2015 &ndash; PRESENT</div>
				</div>
				<p class="content">Create websites for clients using PHP with Drupal and in-house CMS. Work directly with clients to determine project requirements and scope. Developed an Android application. Improved development workflow and build process by introducing Gulp.</p>
				<div class="columns">
					<div class="column is-8">
						<div class="content">
							<h3 class="title is-3">Application Programmer III</h3>
							<p class="subtitle is-4">Eastern Illinois University</p>
						</div>
					</div>
					<div class="column is-4">AUG 2013 &ndash; FEB 2015</div>
				</div>
				<p class="content">Developed multiple web applications with Groovy on Grails for various departments at EIU. Introduced Nexus maven repository to facilitate in-house plugin development for easier code reuse. Developed Java application for synchronizing student photos between two enterprise apps. Regularly provided debugging assistance to junior developers.</p>
				<div class="columns">
					<div class="column is-8">
						<div class="content">
							<h3 class="title is-3">Application Developer</h3>
							<p class="subtitle is-4">Heartland Dental Care</p>
						</div>
					</div>
					<div class="column is-4">MAR 2012 &ndash; AUG 2013</div>
				</div>
				<p class="content">Updated and maintained legacy applications in various languages. Implemented Java-based web services to centralize some commonly-used patterns. Created several Ruby on Rails applications from the ground up. Worked with several APIs to provide additional functionality to enterprise apps.</p>
			</div>
		</div>
	</div>
</section>

<section class="section" id="education">
	<div class="container">
		<div class="columns">
			<div class="column is-4">
				<h2 class="title is-2">Education<a href="#education" class="section-marker"><span class="is-hidden">Jump to education</span></a></h2>
			</div>
			<div class="column is-8">
				<div class="columns">
					<div class="column is-8">
						<div class="content">
							<h3 class="title is-3">BS Computer Science</h3>
							<p class="subtitle is-4">Illinois State University</p>
						</div>
					</div>
					<div class="column is-4">AUG 2008 &ndash; MAY 2011</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--
<section class="section projects" id="portfolio">
	<div class="container">
		<div class="columns">
			<div class="column is-4">
				<h2 class="title is-2">Portfolio<a href="#portfolio" class="section-marker"><span class="is-hidden">Jump to portfolio</span></a></h2>
			</div>
			<div class="column is-8">
				<div class="columns">
					<div class="column is-6">
						<div class="card">
							<div class="card-image">
								<figure class="image is-4by3">
									<img src="{{ asset('img/poe-planner-delete-demo.png') }}" alt="Poe Planner Preview">
								</figure>
							</div>

							<div class="card-content">
								<div class="content">
									<p class="title is-4">PoE Skill Tree Planner</p>
									<p>Open-source, cross-platform, unofficial skill tree planner for Path of Exile.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
  -->

<section class="section" id="blog">
	<div class="container">
		<div class="columns">
			<div class="column is-4">
				<h2 class="title is-2">Blog<a href="#blog" class="section-marker"><span class="is-hidden">Jump to blog</span></a></h2>
			</div>
			<div class="column is-8">
				<ul class="post-list">
					@foreach($posts as $post)
						<li>
							<p class="title is-4"><a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a></p>
							<p class="subtitle is-6">{{ $post->created_at->diffForHumans() }}</p>
						</li>
					@endforeach
				</ul>
				<a href="{{ route('blog.index') }}">More Posts</a>
			</div>
		</div>
	</div>
</section>

<!--
<section class="section" id="contact">
	<div class="container">
		<div class="columns">
			<div class="column is-4">
				<h2 class="title is-2">Contact<a href="#contact" class="section-marker"><span class="is-hidden">Jump to contact</span></a></h2>
			</div>
		</div>
	</div>
</section>
-->

@endsection
